package ch02;

public class Casting {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		float sum = 0.0f;
		sum = 1.5f + 1.6f;
		System.out.println(sum);
		
		int transSum = (int) sum; // 형 변환
		System.out.println(transSum);
		
		char a = '1' ;
		System.out.println(a);
		System.out.println((int) a);
		
		int b = 1000;
		byte b2 = 0;
		b2 = (byte)b;
		System.out.println(b2);
		
		boolean t = false;
		System.out.println("문자열" + t);
		
		
	}

}
