package ch02;

public class Variable {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 숫자 (정수)
		int age; // <= 선언
		age = 10; // <= 초기화
	
		int grade = 4;
		// age : 10, grade : 4
		System.out.println("age : " + age + ", grade : " + grade);
		// age + grade = 14
		System.out.println("age + grade = " +( age + grade));
		
		System.out.println(age);
		System.out.println(age + grade);
		
		System.out.println(10);
		System.out.println(10 + 4);
	}

}
