package ch02;

public class Variable2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// 정수
		int age; // 선언만...
		age = 1; // 초기화가 반드시 있어야 됨
		System.out.println(age + 1);
		
		// 실수
		float score = 1.2f;
		System.out.println(score);
		
		// 문자(1개)
		char ch = 'A';
		// 문자열(여러개)
		String str = "문자여러개";
		String str2 = "일"; // 문자열이지만 1개만 저장
		
		System.out.println(str2 + age + score + ch + str);
		
	}

}
