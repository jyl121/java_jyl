package ch03;

import java.util.concurrent.SynchronousQueue;

public class operator3 {

	public static void main(String[] args) {
		// && &
		int num1 = 0;
		int num2 = 0;
		boolean result = false;
		// & 한개는 무조건 실행- 앞쪽이 거짓이어도 뒤의 것을 살펴봄, 비효율적
		// result = num1++ < 0 & num2++ < 0;
		// && 두개는 상황에 따라 실행- 앞쪽이 거짓이면 뒤의 것을 보지 않음, 효율적 
		// short-circuit evaluation
		result = num1++ < 0 && num2++ < 0;
		System.out.println(result);
		System.out.println("num1: " + num1 + ", num2 : " + num2);
		
		int num3 = 0;
		int num4 = 0;
		result = ++num3 > 0 || ++num4 > 0;
		System.out.println(result);
		System.out.println("num3 : " + num3 + ", num4 : " + num4);
	}

}
