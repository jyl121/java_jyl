
package ch03;

public class Exam3_5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int num1 = 0, num2 = 0, num3 = 0, num4 = 0;
		int num5 = 0, num6 = 0, num7 = 0, num8 = 0;

		int result1 = (num1++) + (num2++);
		int result2 = (++num3) + (num4++); // ++ 앞에 있으면 무조건 먼저 연산, ++ 뒤에 있으면 줄이 끝난 다음에 연산됨
		int result3 = (num5++) + (++num6);
		int result4 = (++num7) + (++num8);

		System.out.println(num1); // 1
		System.out.println(num2++); // 1
		
		System.out.println(result1); // 0
		System.out.println(result2); // 1
		System.out.println(result3); // 1
		System.out.println(result4); // 2

	}

}
