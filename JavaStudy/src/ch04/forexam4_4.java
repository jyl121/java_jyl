package ch04;

public class forexam4_4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		boolean isContinue = true; // 반복문 진행 여부
		int count = 0; // 눈 10이 나오기 전까지 주사위를 던진 횟수
		while(isContinue) {
			int num1 = (int) (Math.random()*6 + 1); // 1~6까지의 랜덤 숫자
			int num2 = (int) (Math.random()*6 + 1); // 1~6까지의 랜덤 숫자
			/*1. (눈1, 눈2) 형태 출력 코드 */
			System.out.println("(" + num1 + "," + num2 + ")");

			count ++;			
			/*2. 눈1+눈2의 값이 10인 경우 반복문 종료 코드*/
		
			if (num1+num2==10){
				isContinue = false;
				break;
			}	
				
			System.out.println("주사위를 던진 횟수 :" + count);
		}
	}	
}
