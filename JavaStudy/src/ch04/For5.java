package ch04;

import java.util.Random;

public class For5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// 랜덤 방법1
		Random random1 = new Random();
		// 1~45까지의 난수
		int num1 = random1.nextInt(45) + 1;
		System.out.println(num1);
		
		// 랜덤 방법2
		
		// 0.0~0.99999999999999
		// 0.1 => 1
		//0.7 => 7
		//0.9999 => 9
		// 0 - 10 사이의 랜덤 수 구하기
		int num2 = (int)(Math.random() * 45) + 1;
		//0~44까지의 정수이기 때문에 1을 더해줌			
	}

}
