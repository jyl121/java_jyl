package ch04;

import java.util.Scanner;

public class if2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int score = 0;
		
		// 문구 입력
		System.out.println("점수를 입력해주세요. (숫자만, 양수만) >> ");
		
		// 키보드를 이용하여 값 입력 - 라이브러리 = API
		Scanner scan = new Scanner(System.in);
		score = scan.nextInt();
	
		if(score >= 90) {
			System.out.println("A");
		} else if(score >= 80) { 
			System.out.println("B");
		} else if(score >= 70) {
			System.out.println("C");
		} else if(score >= 60) {
			System.out.println("D");
		} else {
			System.out.println("F");
		
		}
	}
}
